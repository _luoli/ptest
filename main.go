package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/robfig/cron"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

const secret = "1lGTO2zAFoWB9j8XUVXHwd"

func main() {
	cron2 := cron.New() //创建一个cron实例

	//执行定时任务（每5秒执行一次）
	err:= cron2.AddFunc("1 50 8,14,20 * * 1,2,3,4,5", CreatOrder) //"1 50 8,21,14 * * 1,2,3,4,5"
	if err!=nil{
		fmt.Println(err)
	}

	//启动/关闭
	cron2.Start()
	defer cron2.Stop()
	select {
	//查询语句，保持程序运行，在这里等同于for{}
	}
}

func CreatOrder() {

	group := make(map[string]string)
	group["1"] = "https://open.feishu.cn/open-apis/bot/v2/hook/1434c30d-f9dc-4386-8cac-f8d81aec879d"
	times := time.Now()
	timetamp := strconv.Itoa(int(times.Unix()))
	timeYHM := times.Format("2006-01-02 15:04:05")
	GenSign := func(secret string, timestamp int64) (string, error) {
		//timestamp + key 做sha256, 再进行base64 encode
		stringToSign := fmt.Sprintf("%v", timestamp) + "\n" + secret
		var data []byte
		h := hmac.New(sha256.New, []byte(stringToSign))
		_, err := h.Write(data)
		if err != nil {
			return "", err
		}
		signature := base64.StdEncoding.EncodeToString(h.Sum(nil))
		return signature, nil
	}

	sign, _ := GenSign(secret, times.Unix())

	//content := fmt.Sprintf("{\"text\":\"<at user_id=\"all\">所有人</at> 宝子们，%s了，要点餐了\"}", timeYHM)
	content := fmt.Sprintf("{\"text\":\"<at user_id=\\\"all\\\">所有人</at> 宝子们, 现在时间%s, 记得点餐了\"}", timeYHM)
	//fmt.Println(content)
	body := map[string]string{
		"content":   content,
		"msg_type":  "text",
		"timestamp": timetamp,
		"sign":      sign,
	}

	jsonStr, _ := json.Marshal(body)

	fmt.Println(string(jsonStr))

	for _, groupurl := range group {
		client := &http.Client{}
		req, err := http.NewRequest("POST", groupurl, strings.NewReader(string(jsonStr)))

		req.Header.Add("Content-Type", "application/json")

		if err != nil {
			fmt.Printf("post failed, err:%v\n", err)
			return
		}
		resp, err := client.Do(req)
		if err != nil {
			fmt.Println(err)
		}
		response, _ := ioutil.ReadAll(resp.Body)
		fmt.Println(string(response))
	}
}
